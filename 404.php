<?php get_header(); ?>
	
	<div id="content">

		<div id="inner-content" class="wrap">

			<main id="main" class="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="https://schema.org/Blog">

				<article id="post-not-found" class="hentry">

					<div class="content">
						<div class="content__container" itemprop="articleBody">

							<header class="article-header">

								<?php get_template_part( 'templates/header', 'title'); ?>

							</header>

							<section class="entry-content">

								<div class="404-txt text-center">

									<h3>Die aufgerufene Seite existiert nicht</h3>
									<p>Wir konnten die Seite zu dieser URL leider nicht finden.</p>

								</div>

							</section>
					
						</div>
					</div>

				</article>

			</main>

		</div>

	</div>

<?php get_footer(); ?>
