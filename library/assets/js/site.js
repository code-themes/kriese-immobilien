// @codekit-prepend "../../../node_modules/popper.js/dist/umd/popper.min.js"
// @codekit-prepend "../../../node_modules/bootstrap/dist/js/bootstrap.min.js"
// @codekit-prepend "../../../node_modules/autosize/dist/autosize.min.js"

/* global autosize, jQuery */

jQuery(document).ready(function($) {
    // Autosize
    autosize($('textarea'));
});
