            </div>
            
            <footer class="footer" role="contentinfo" itemscope itemtype="https://schema.org/WPFooter">
                <div class="footer__container">

                    <div class="footer__row footer__row--1">
                        <div class="footer__col footer__col--opening-hours">

                        	<?php if (have_rows('footer_opening_hours', 'options')): ?>
	                            <div class="footer__opening-hours">
	                                <h6 class="footer__opening-hours-heading">Öffnungszeiten</h6>
	                                <table class="footer__opening-hours-table">
	                                    <tbody>
	                                    	<?php while (have_rows('footer_opening_hours', 'options')): the_row(); ?>
		                                        <tr>
		                                            <th><?php the_sub_field('day'); ?></th>
		                                            <td><?php the_sub_field('time'); ?></td>
		                                        </tr>
		                                    <?php endwhile; ?>
	                                    </tbody>
	                                </table>
	                            </div>
	                        <?php endif; ?>

                        </div>
                        <div class="footer__col footer__col--contact-link">

                        	<?php $footer_contact_form = get_field('footer_contact_form', 'options');
                        	if (!empty($footer_contact_form)): ?>
	                            <div class="footer__contact-link-wrapper">
	                                <a href="<?php echo $footer_contact_form; ?>" class="footer__contact-link">Kontaktformular</a>
	                            </div>
	                        <?php endif; ?>

                        </div>
                        <div class="footer__col footer__col--social-links">

                        	<?php if (have_rows('footer_social_links', 'options')): ?>
	                            <nav class="footer__social-links">
	                                <ul>
	                                	<?php while (have_rows('footer_social_links', 'options')): the_row(); ?>
	                                		<?php $service = get_sub_field('service'); ?>
		                                    <li class="<?php echo $service['value']; ?>"><a href="<?php the_sub_field('url'); ?>" target="_blank"><?php echo $service['label']; ?></a></li>
	                                    <?php endwhile; ?>
	                                </ul>
	                            </nav>
							<?php endif; ?>

                        </div>
                        <div class="footer__col footer__col--partner-logo">
                    
                    		<?php if (get_field('footer_show_ivd', 'options')): ?>
	                            <div class="footer__partner-logo-wrapper">
	                                <div class="footer__partner-logo">Mitglied im ivd</div>
	                            </div>
	                        <?php endif; ?>

                        </div>
                    </div>

                    <div class="footer__row footer__row--2">
                        <div class="footer__col footer__col--links">

                            <nav class="footer__links">
                                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                                <?php wp_nav_menu(array(
                                    'menu' => __('Footer Links', 'platetheme'),
                                    'menu_class' => '',
                                    'container' => false,
                                    'container_class' => '',
                                    'depth' => 1,
                                    'theme_location' => 'footer-links',
                                )); ?>
                            </nav>

                        </div>
                        <div class="footer__col footer__col--copyright">
                    
                    		<?php $footer_text = get_field('footer_text', 'options');
                    		if (!empty($footer_text)): ?>
	                            <div class="footer__copyright">
	                                <?php echo $footer_text; ?>
	                            </div>
	                        <?php endif; ?>

                        </div>
                    </div>

                </div>
            </footer>

        </div>

		<?php // all js scripts are loaded in library/functions.php ?>
		<?php wp_footer(); ?>

	</body>
</html>
