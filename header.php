<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_schema(); ?> <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>

        <?php /**
         * updated with non-blocking order
         * see here: https://csswizardry.com/2018/11/css-and-network-performance/
         * 
         * In short, place any js here that doesn't need to act on css before any css to
         * speed up page loads.
         */
        ?>

        <?php // drop Google Analytics here ?>
        <?php // end analytics ?>

        <?php // See everything you need to know about the <head> here: https://github.com/joshbuchea/HEAD ?>
        <meta charset='<?php bloginfo( 'charset' ); ?>'>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php // favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
        <link rel="icon" href="<?php echo get_theme_file_uri(); ?>/favicon.png">
        <!--[if IE]>
            <link rel="shortcut icon" href="<?php echo get_theme_file_uri(); ?>/favicon.ico">
        <![endif]-->

        <!-- Apple Touch Icon -->
        <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri(); ?>/library/images/apple-touch-icon.png">

        <?php // updated pingback. Thanks @HardeepAsrani https://github.com/HardeepAsrani ?>
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
            <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php endif; ?>

        <?php // put font scripts like Typekit/Adobe Fonts here ?>
        <?php // end fonts ?>

        <?php // wordpress head functions ?>
        <?php wp_head(); ?>
        <?php // end of wordpress head ?>

    </head>
	<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

        <div id="page">

            <header class="header" role="banner" itemscope itemtype="https://schema.org/WPHeader">
                <div class="header__container">
                    <a href="<?php echo home_url(); ?>" rel="nofollow" itemprop="url" class="logo header__logo"><?php bloginfo('name'); ?></a>

                    <nav class="main-menu header__main-menu" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="<?php _e( 'Primary Menu ', 'platetheme' ); ?>">
                        <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                        <?php wp_nav_menu(array(
                            'menu' => __('The Main Menu', 'platetheme'),
                            'menu_class' => '',
                            'container' => false,
                            'container_class' => '',
                            'depth' => 2,
                            'theme_location' => 'main-nav',
                        )); ?>
                    </nav>

                    <label for="hamburger" class="mobile-menu-button header__mobile-menu-button"><span class="mobile-menu-button__hamburger"></span> Menü</label>
                </div>
            </header>

            <input type="checkbox" id="hamburger" class="mobile-menu-checkbox">
            <nav class="mobile-menu">
                <label for="hamburger" class="mobile-menu__close"><span class="mobile-menu__close-icon"></span></label>
                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                <?php wp_nav_menu(array(
                    'menu' => __('The Main Menu', 'platetheme'),
                    'menu_class' => '',
                    'container' => false,
                    'container_class' => '',
                    'depth' => 1,
                    'theme_location' => 'main-nav',
                )); ?>
            </nav>

            <div class="main">
