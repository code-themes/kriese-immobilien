<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'about';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$text_1 = get_field('text_1');
$text_2 = get_field('text_2');
$text_3 = get_field('text_3');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="about__container">

		<?php if (!empty($heading)): ?>
			<div class="about__header">
				<h2 class="about__heading"><?php echo $heading; ?></h2>
			</div>
		<?php endif; ?>

		<div class="about__row">

			<?php if (!empty($text_1)): ?>
				<div class="about__col">
					<?php echo $text_1; ?>
				</div>
			<?php endif; ?>

			<?php if (!empty($text_2)): ?>
				<div class="about__col">
					<?php echo $text_2; ?>
				</div>
			<?php endif; ?>

			<?php if (!empty($text_3)): ?>
				<div class="about__col">
					<?php echo $text_3; ?>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
