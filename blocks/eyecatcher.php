<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'eyecatcher';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$link = get_field('button');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="eyecatcher__container">

		<h1 class="eyecatcher__heading"><?php echo $heading; ?></h1>

		<?php if (!empty($link)): ?>
			<div class="eyecatcher__button-wrapper">
				<a href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link['target'] ? $link['target'] : '_self'); ?>" class="eyecatcher__button"><?php echo esc_html($link['title']); ?></a>
			</div>
		<?php endif; ?>

	</div>
</div>
<style type="text/css">
	.header {
		background: transparent;
	}

	<?php if (is_admin()): ?>
		#<?php echo esc_attr($id); ?> {
			margin-top: 0;
		}
	<?php endif; ?>
</style>
