<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'contact-options';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="contact-options__container">

		<?php if (!empty($heading)): ?>
			<div class="contact-options__header">
				<h2 class="contact-options__heading"><?php echo $heading; ?></h2>
			</div>
		<?php endif; ?>

		<?php if (have_rows('items')): ?>
			<div class="contact-options__row">
				<?php while (have_rows('items')): the_row(); ?>
					<div class="contact-options__col">

						<div class="contact-option">

							<?php $icon = get_sub_field('icon');
							if (!empty($icon)): ?>
								<?php $icon_retina = get_sub_field('icon_retina'); ?>
								<div class="contact-option__icon-wrapper">
									<img src="<?php echo esc_url($icon['url']); ?>" srcset="<?php echo esc_url($icon['url']); ?> 1x<?php echo !empty($icon_retina) ? ', '.esc_url($icon_retina['url']).' 2x' : ''; ?>" alt="<?php echo esc_attr($icon['alt']); ?>" class="contact-option__icon">
								</div>
							<?php endif; ?>

							<div class="contact-option__body">

								<?php $title = get_sub_field('title');
								if (!empty($title)): ?>
									<h3 class="contact-option__heading"><?php echo $title; ?></h3>
								<?php endif; ?>

								<?php $text = get_sub_field('text');
								if (!empty($text)): ?>
									<p class="contact-option__text"><?php echo $text; ?></p>
								<?php endif; ?>

							</div>
						</div>

					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
