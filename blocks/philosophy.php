<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'philosophy';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$background = get_field('background');
if (!empty($background)) {
	$className .= ' philosophy--'.$background;
}

$i = 0;

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<?php if (have_rows('slides')): ?>
		<div id="carousel1" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<?php $i = 0; while (have_rows('slides')): the_row(); ?>
					<div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
						<div class="carousel-caption">
							<div class="philosophy__container">

								<?php $heading = get_sub_field('heading');
								if (!empty($heading)): ?>
									<div class="philosophy__header-row">
										<div class="philosophy__header-col">
											<h1 class="philosophy__heading"><?php echo $heading; ?></h1>
										</div>
									</div>
								<?php endif; ?>

								<?php $text = get_sub_field('text');
								if (!empty($text)): ?>
									<div class="philosophy__content-row">
										<div class="philosophy__content-col">
											<div class="philosophy__content">
												<?php echo $text; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>

							</div>
						</div>
					</div>
				<?php $i++; endwhile; ?>
			</div>
			<?php if ($i > 1): ?>
				<a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			<?php endif; ?>
		</div>
	<?php endif; ?>

</div>
