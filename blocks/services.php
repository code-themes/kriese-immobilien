<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'services';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$icon_1 = get_field('icon_1');
$icon_2 = get_field('icon_2');
$icon_retina_1 = get_field('icon_retina_1');
$icon_retina_2 = get_field('icon_retina_2');
$text_1 = get_field('text_1');
$text_2 = get_field('text_2');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="services__container">

		<div class="services__row">
			<div class="services__col">

				<?php if (!empty($icon_1)): ?>
					<div class="services__image-wrapper">
						<img src="<?php echo esc_url($icon_1['url']); ?>" srcset="<?php echo esc_url($icon_1['url']); ?> 1x<?php echo !empty($icon_retina_1) ? ', '.esc_url($icon_retina_1['url']).' 2x' : ''; ?>" alt="<?php echo esc_attr($icon_1['alt']); ?>" class="services__image">
					</div>
				<?php endif; ?>

				<?php if (!empty($text_1)): ?>
					<div class="services__text"><?php echo $text_1; ?></div>
				<?php endif; ?>

			</div>
			<div class="services__col">

				<?php if (!empty($icon_2)): ?>
					<div class="services__image-wrapper">
						<img src="<?php echo esc_url($icon_2['url']); ?>" srcset="<?php echo esc_url($icon_2['url']); ?> 1x<?php echo !empty($icon_retina_2) ? ', '.esc_url($icon_retina_2['url']).' 2x' : ''; ?>" alt="<?php echo esc_attr($icon_2['alt']); ?>" class="services__image">
					</div>
				<?php endif; ?>

				<?php if (!empty($text_2)): ?>
					<div class="services__text services__text--orange"><?php echo $text_2; ?></div>
				<?php endif; ?>

			</div>
		</div>

	</div>
</div>
