<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'map';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$map = get_field('map');
$image = get_field('image');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="map__container">

		<div class="map__row">
			<div class="map__col">

				<div class="map__map-wrapper">
					<div id="map" class="map__map"></div>
				</div>

			</div>
			<div class="map__col">

				<?php if (!empty($image)): ?>
					<div class="map__image-wrapper">
						<?php echo wp_get_attachment_image($image['ID'], 'full', false, array('class' => 'map__image')); ?>
					</div>
				<?php endif; ?>

			</div>
		</div>

	</div>
</div>
<?php if (!empty($map['address'])): ?>
	<script type="text/javascript">
		var geocoder, map, marker;

		function initMap() {
			geocoder = new google.maps.Geocoder();

			geocoder.geocode({
				'address': '<?php echo esc_html($map['address']); ?>'
			}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					map = new google.maps.Map(document.getElementById('map'), {
						center: results[0].geometry.location,
						zoom: <?php echo $map['zoom']; ?>
					});

					marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location
					});
				}
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2SkTceUlSsQc7yvdMSEjgNyYl2JDYsSc&callback=initMap"
	    async defer></script>
<?php endif; ?>
