<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'images';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$image_1 = get_field('image_1');
$image_2 = get_field('image_2');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="images__container">

		<div class="images__row">
			<div class="images__col">

				<?php if (!empty($image_1)): ?>
					<div class="images__image-wrapper">
						<?php echo wp_get_attachment_image($image_1['ID'], 'full', false, array('class' => 'images__image')); ?>
					</div>
				<?php endif; ?>

			</div>
			<div class="images__col">

				<?php if (!empty($image_2)): ?>
					<div class="images__image-wrapper">
						<?php echo wp_get_attachment_image($image_2['ID'], 'full', false, array('class' => 'images__image')); ?>
					</div>
				<?php endif; ?>

			</div>
		</div>

	</div>
</div>
