<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'team';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="team__container">

		<?php if (!empty($heading)): ?>
			<div class="team__header">
				<h2 class="team__heading"><?php echo $heading; ?></h2>
			</div>
		<?php endif; ?>

		<?php if (have_rows('people')): ?>
			<div class="team__items">
				<?php while (have_rows('people')): the_row(); ?>
					<div class="team__item">

						<div class="person">
							<div class="person__row">
								<div class="person__col">

									<?php $image = get_sub_field('photo');
									if (!empty($image)): ?>
										<div class="person__image-wrapper">
											<?php echo wp_get_attachment_image($image['ID'], 'full', false, array('class' => 'person__image')); ?>
										</div>
									<?php endif; ?>

								</div>
								<div class="person__col">

									<?php $name = get_sub_field('name');
									if (!empty($name)): ?>
										<h3 class="person__heading"><?php echo $name; ?></h3>
									<?php endif; ?>

									<?php $text = get_sub_field('text');
									if (!empty($text)): ?>
										<div class="person__text-wrapper">
											<?php echo $text; ?>
										</div>
									<?php endif; ?>

								</div>
							</div>
						</div>

					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
