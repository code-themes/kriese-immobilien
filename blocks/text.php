<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'text';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$heading_color = get_field('heading_color');
$text = get_field('text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="text__container">

		<?php if (!empty($heading)): ?>
			<div class="text__header">
				<h1 class="text__heading text__heading--<?php echo $heading_color; ?>"><?php echo $heading; ?></h1>
			</div>
		<?php endif; ?>

		<?php if (!empty($text)): ?>
			<div class="text__content">
				<?php echo $text; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
