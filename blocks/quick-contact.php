<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'quick-contact';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$image_1 = get_field('image_1');
$image_2 = get_field('image_2');
$image_3 = get_field('image_3');
$title_1 = get_field('title_1');
$title_2 = get_field('title_2');
$title_3 = get_field('title_3');
$text_1 = get_field('text_1');
$text_2 = get_field('text_2');
$text_3 = get_field('text_3');
$contact_link = get_field('contact_link');
$phone_link = get_field('phone_link');
$consulting_link = get_field('consulting_link');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="quick-contact__container">

		<?php if (!empty($heading)): ?>
			<div class="quick-contact__header">
				<h2 class="quick-contact__heading"><?php echo $heading; ?></h2>
			</div>
		<?php endif; ?>

		<div class="quick-contact__row">
			<div class="quick-contact__col quick-contact__col--1">
				<div class="quick-contact__item-row">

					<div class="quick-contact__item-col">
						<?php if (!empty($image_1)): ?>
							<?php echo wp_get_attachment_image($image_1['ID'], 'full', false, array('class' => 'quick-contact__item-image')); ?>
						<?php endif; ?>

						<?php if (!empty($title_1)): ?>
							<h3 class="quick-contact__item-heading"><?php echo $title_1; ?></h3>
						<?php endif; ?>

						<?php if (!empty($text_1)): ?>
							<p class="quick-contact__item-text"><?php echo $text_1; ?></p>
						<?php endif; ?>
					</div>

					<div class="quick-contact__item-col">
						<?php if (!empty($image_2)): ?>
							<?php echo wp_get_attachment_image($image_2['ID'], 'full', false, array('class' => 'quick-contact__item-image')); ?>
						<?php endif; ?>

						<?php if (!empty($title_2)): ?>
							<h3 class="quick-contact__item-heading"><?php echo $title_2; ?></h3>
						<?php endif; ?>

						<?php if (!empty($text_2)): ?>
							<p class="quick-contact__item-text"><?php echo $text_2; ?></p>
						<?php endif; ?>
					</div>

					<div class="quick-contact__item-col">
						<?php if (!empty($image_3)): ?>
							<?php echo wp_get_attachment_image($image_3['ID'], 'full', false, array('class' => 'quick-contact__item-image')); ?>
						<?php endif; ?>

						<?php if (!empty($title_3)): ?>
							<h3 class="quick-contact__item-heading"><?php echo $title_3; ?></h3>
						<?php endif; ?>

						<?php if (!empty($text_3)): ?>
							<p class="quick-contact__item-text"><?php echo $text_3; ?></p>
						<?php endif; ?>
					</div>

				</div>
			</div>
			<div class="quick-contact__col quick-contact__col--2">

				<div class="quick-contact__links">
					<div class="quick-contact__options">

						<?php if (!empty($contact_link)): ?>
							<a href="<?php echo esc_url($contact_link['url']); ?>" target="<?php echo esc_attr($contact_link['target'] ? $contact_link['target'] : '_self'); ?>" class="quick-contact__option quick-contact__option--contact"><?php echo esc_html($contact_link['title']); ?></a>
						<?php endif; ?>

						<?php if (!empty($phone_link)): ?>
							<a href="<?php echo esc_url($phone_link['url']); ?>" target="<?php echo esc_attr($phone_link['target'] ? $phone_link['target'] : '_self'); ?>" class="quick-contact__option quick-contact__option--phone"><?php echo esc_html($phone_link['title']); ?></a>
						<?php endif; ?>

					</div>

					<?php if (!empty($consulting_link)): ?>
						<div class="quick-contact__consulting-wrapper">
							<a href="<?php echo esc_url($consulting_link['url']); ?>" target="<?php echo esc_attr($consulting_link['target'] ? $consulting_link['target'] : '_self'); ?>" class="quick-contact__consulting"><?php echo esc_html($consulting_link['title']); ?></a>
						</div>
					<?php endif; ?>
				</div>

			</div>
		</div>

	</div>
</div>
