<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'business-areas';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
// ...

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="business-areas__container">

		<?php if (have_rows('items')): ?>
			<div class="business-areas__items">
				<?php while (have_rows('items')): the_row(); ?>
					<div class="business-areas__item">
						<div class="business-area">

							<?php $icon = get_sub_field('icon');
							if (!empty($icon)): ?>
								<div class="business-area__icon business-area__icon--<?php echo $icon; ?>"></div>
							<?php endif; ?>

							<div class="business-area__content-wrapper">
								<div class="business-area__content">

									<?php $heading = get_sub_field('heading');
									if (!empty($heading)): ?>
										<h3 class="business-area__heading"><?php echo $heading; ?></h3>
									<?php endif; ?>

									<?php $text = get_sub_field('text');
									if (!empty($text)): ?>
										<p class="business-area__text">
											<?php echo $text; ?>
										</p>
									<?php endif; ?>

								</div>

								<?php $heading_2 = get_sub_field('heading_2');
								if (!empty($heading_2)): ?>
									<h2 class="business-area__heading-2"><?php echo $heading_2; ?></h2>
								<?php endif; ?>

							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
