<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'contact-form';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$text = get_field('text');
$form = get_field('form');

$parking = get_field('parking');
$parkingHtml = '';
if (!empty($parking['title']) || !empty($parking['text'])) {
	$parkingIconPath = get_template_directory_uri();

	$parkingHtmlTitle = '';
	if (!empty($parking['title'])) {
		$parkingHtmlTitle = <<<EOS
<h3 class="parking__heading">{$parking['title']}</h3>
EOS;
	}

	$parkingHtmlText = '';
	if (!empty($parking['title'])) {
		$parkingHtmlText = <<<EOS
<p class="parking__text">{$parking['text']}</p>
EOS;
	}

	$parkingHtml = <<<EOS
<div class="parking contact-form__parking">
	<div class="parking__icon-wrapper">
		<img src="{$parkingIconPath}/library/assets/images/contact-form/icon-parking.png" srcset="{$parkingIconPath}/library/assets/images/contact-form/icon-parking.png 1x, {$parkingIconPath}/library/assets/images/contact-form/icon-parking@2x.png 2x" class="parking__icon">
	</div>
	<div class="parking__body">
		{$parkingHtmlTitle}
		{$parkingHtmlText}
	</div>
</div>
EOS;
}

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="contact-form__container">

		<?php if (!empty($heading)): ?>
			<div class="contact-form__header">
				<h2 class="contact-form__heading"><?php echo $heading; ?></h2>
			</div>
		<?php endif; ?>

		<?php if (!empty($text)): ?>
			<div class="contact-form__text-wrapper">
				<p class="contact-form__text"><?php echo $text; ?></p>
			</div>
		<?php endif; ?>

		<?php if (!empty($form)): ?>
			<div class="contact-form__form">
				<?php echo str_replace(array('{parking}'), array($parkingHtml), do_shortcode($form)); ?>
			</div>
		<?php endif; ?>

	</div>
</div>
