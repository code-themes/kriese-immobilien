<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'properties';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="properties__container">

		<?php if (!empty($heading)): ?>
			<div class="properties__header">
				<h2 class="properties__heading"><?php echo $heading; ?></h2>
			</div>
		<?php endif; ?>

		<?php if (have_rows('items')): ?>
			<div class="properties__items">
				<?php while (have_rows('items')): the_row(); ?>
					<div class="properties__item">

						<div class="property">
							<div class="property__row">
								<div class="property__col">

									<?php $image = get_sub_field('image');
									if (!empty($image)): ?>
										<div class="property__image-wrapper">
											<?php echo wp_get_attachment_image($image['ID'], 'full', false, array('class' => 'property__image')); ?>
										</div>
									<?php endif; ?>

								</div>
								<div class="property__col">

									<?php $title = get_sub_field('title');
									if (!empty($title)): ?>
										<h3 class="property__heading"><?php echo $title; ?></h3>
									<?php endif; ?>

									<?php $description = get_sub_field('description');
									if (!empty($description)): ?>
										<p class="property__text"><?php echo $description; ?></p>
									<?php endif; ?>

									<?php $link = get_sub_field('link');
									if (!empty($link)): ?>
										<div class="property__link-wrapper">
											<a href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link['target'] ? $link['target'] : '_self'); ?>" class="property__link"><?php echo esc_html($link['title']); ?></a>
										</div>
									<?php endif; ?>

								</div>
							</div>
						</div>

					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
