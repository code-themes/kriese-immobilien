<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'history';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
// ...

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="history__container">

		<?php if (have_rows('items')): ?>
			<?php while (have_rows('items')): the_row(); ?>
				<div class="history__row">
					<div class="history__col history__col--1">

						<?php $image = get_sub_field('image');
						if (!empty($image)): ?>
							<div class="history__image-wrapper">
								<?php echo wp_get_attachment_image($image['ID'], 'full', false, array('class' => 'history__image')); ?>
							</div>
						<?php endif; ?>

					</div>
					<div class="history__col history__col--2">

						<?php $heading = get_sub_field('heading');
						if (!empty($heading)): ?>
							<h3 class="history__heading"><?php echo $heading; ?></h3>
						<?php endif; ?>

						<?php $text = get_sub_field('text');
						if (!empty($text)): ?>
							<div class="history__text-wrapper">
								<?php echo $text; ?>
							</div>
						<?php endif; ?>

					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>

	</div>
</div>
